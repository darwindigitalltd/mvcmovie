using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MVCMovie.Helper
{
  public class JsonHelper
  {

    public JsonSerializerSettings serializerSettings = new JsonSerializerSettings();

    public JsonHelper()
    {
      serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
    }
  }
}
