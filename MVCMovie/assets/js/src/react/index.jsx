import React from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import MovieIndex from './Movie/MovieIndex';
import CreateMovie from './Movie/CreateMovie';
import EditMovie from './Movie/EditMovie';

import {createStore} from 'redux';
import rootReducer from './redux-config/root-reducer';
import {Provider} from 'react-redux';
import store from './redux-config/store';

const getRenderFunc = app => {
  return () => {
    render(app.component, app.element, app.name, app.hydrate);
  };
};

const render = (Component, element, name, hydrate = false) => {
  const renderOrHydrate = hydrate ? ReactDOM.hydrate : ReactDOM.render;
  renderOrHydrate(
    <AppContainer name={name}>
      {Component}
    </AppContainer>,
    element
  );
};
const elements = {
  movieIndex: document.getElementsByClassName('movie-index__react-anchor'),
  createMovie: document.getElementsByClassName('create-movie__react-anchor'),
  editMovie: document.getElementsByClassName('edit-movie__react-anchor')
};

const apps = [];

if (elements.movieIndex.length > 0) {
  apps.push(...Array.from(elements.movieIndex).map(element => ({
    element,
    component: <Provider store={store}><MovieIndex movies={element.getAttribute('data-movies')}/></Provider>,
    file: './Movie/MovieIndex',
    name: 'MovieIndex'
  })));
}

if (elements.createMovie.length > 0) {
  apps.push(...Array.from(elements.createMovie).map(element => ({
    element,
    component: <CreateMovie/>,
    file: './Movie/CreateMovie',
    name: 'CreateMovie'
  })));
}

if (elements.editMovie.length > 0) {
  apps.push(...Array.from(elements.editMovie).map(element => ({
    element,
    component: <EditMovie/>,
    file: './Movie/EditMovie',
    name: 'EditMovie'
  })));
}



for (const app of apps) {
  app.element ? getRenderFunc(app)() : null;
  module.hot ? module.hot.accept(getRenderFunc(app)()) : null;
}
