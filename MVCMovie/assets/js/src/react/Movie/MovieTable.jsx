import React from 'react';
import MovieRow from './MovieRow.jsx';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
  console.log(state);
  const movieState = state.get('movies');
  return {
    genre: movieState.get('genre')
  };
};

const MovieTable = (props) => {

  const movies = JSON.parse(props.movies);


  return (
    <table className={'table'}>
      <thead>
      <tr className={'table__row'}>
        <th className={'table__row__header'}>
          Title
        </th>
        <th className={'table__row__header'}>
          Release Date
        </th>
        <th className={'table__row__header'}>
          Genre
        </th>
        <th className={'table__row__header'}>
          Price
        </th>
        <th className={'table__row__header'}>
          Rating
        </th>
        <th className={'table__row__empty'}></th>
      </tr>
      </thead>

      {movies.filter((movie) => props.genre === '' ? movie : movie.genre === props.genre).map((movie) => {
        return <MovieRow {...movie} />;
      })}
    </table>
  );
};

export default connect(mapStateToProps)(MovieTable);
