import React, {useState} from 'react';
import request from 'superagent';
import TextField from '@material-ui/core/TextField';
import validate from 'validate.js';

/*
TODO:
 Add form validation
 Check forms before submitting
 Hide releaseDate when not clicked
 https://moduscreate.com/blog/reactjs-form-validation-approaches/


 */


const CreateMovie = () => {

  const [formFields, setFormFields] = useState({
    title: '',
    rating: '',
    releaseDate: '',
    genre: '',
    price: ''
  });

  const [validation, setValidation] = useState({
    valid: false,
    title: {
      valid: false,
      errorMsg: ''
    }
  });


  const constraints = {
    title: {
      length : {
        minimum: 3,
        maximum: 100
      }
    }
  }

  const [styleClass, setStyleClass] = useState({
    title: ''
  });

  const updateField =
    (key) => e => {
      //Check Validation
      if (validate({title: e.target.value}, constraints)) {
        setStyleClass.title = 'form_field'
      }

      const fields = Object.assign({}, formFields);
      fields[key] = e.target.value;
      setFormFields(fields);
    };

  const submitForm = e => {
    console.log(formFields);
    if (validation.valid) {
      e.preventDefault();
      request.post('/Movies/Create').send(formFields).then(() => window.location.href = '/movies/index');
    }

  };

  return (
    <form className={'form'} onSubmit={submitForm} method={'POST'}>
      <div className={styleClass.title}>
        <TextField error={false}
                   label={'Title'}
                   className={'form__field__text-field'}
                   type={'text'}
                   onChange={updateField('title')}
        />
      </div>

      <div className={'form__field'}>
        <TextField error={false}
                   label={'Rating'}
                   className={'form__field__text-field'}
                   type={'text'}
                   onChange={updateField('rating')}
        />
      </div>

      <div className={'form__field'}>
        <TextField error={false}
                   label={'Release Date'}
                   className={'form__field__text-field'}
                   type={'date'}
                   onChange={updateField('releaseDate')}
        />
      </div>

      <div className={'form__field'}>
        <TextField error={false}
                   label={'Genre'}
                   className={'form__field__text-field'}
                   type={'text'}
                   onChange={updateField('genre')}
        />
      </div>

      <div className={'form__field'}>
        <TextField error={false}
                   label={'Price'}
                   className={'form__field__text-field'}
                   type={'text'}
                   onChange={updateField('price')}
        />
      </div>
      <button type={'submit'} className={'form__button'}>Create Movie</button>
    </form>
  );
};

export default CreateMovie;
