import React from 'react';
import moment from 'moment';


const MovieRow = ({title, releaseDate, genre, price, rating, id}) => {
  return (
    <tbody>
      <tr className='table__row'>
        <td className='table__row__data'>
          {title}
        </td>
        <td className='table__row__data'>
          {moment(releaseDate).format('YYYY-MM-DD')}
        </td>
        <td className='table__row__data'>
          {genre}
        </td>
        <td className='table__row__data'>
          £{price}
        </td>
        <td className='table__row__data'>
          {rating}
        </td>
        <td className='table__row__links'>
          <a href={`/Movies/Edit/${id}`}>Edit | </a>
          <a href={`/Movies/Details/${id}`}>Details | </a>
          <a href={`/Movies/Delete/${id}`}>Delete</a>
        </td>
      </tr>
    </tbody>
  );
};

export default MovieRow;
