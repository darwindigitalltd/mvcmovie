import React from 'react';

const EditMovie = (props) => {

  const movies = JSON.parse(props.movies);
  return (
    <form className={'form'}>
      <h1>Hello</h1>
      <div className={'form__field'}>
        <input className={'form__field__input'} type={'text'} placeholder={'Title'}/>
      </div>

      <div className={'form__field'}>
        <input className={'form__field__input'} type={'text'} placeholder={'Rating'}/>
      </div>

      <div className={'form__field'}>
        <input className={'form__field__input'} type={'text'} placeholder={'Release Date'}/>
      </div>

      <div className={'form__field'}>
        <input className={'form__field__input'} type={'text'} placeholder={'Genre'}/>
      </div>

      <div className={'form__field'}>
        <input className={'form__field__input'} type={'text'} placeholder={'Price'}/>
      </div>

      <button type={'submit'} className={'form__button'}>Create Movie</button>
    </form>
  );
};

export default EditMovie;
