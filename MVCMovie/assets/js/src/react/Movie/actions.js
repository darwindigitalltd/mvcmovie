const setGenre = (genre) =>
  ({
    type: 'SET_GENRE',
    payload: {genre}
  });

export default setGenre;
