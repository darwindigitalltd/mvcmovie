import {Map} from 'immutable';

const reducer = (state = Map({genre: ''}), action) => {

  switch (action.type) {
    case 'SET_GENRE' :

      return state.set('genre', action.payload.genre);
    default:
      return state;
  }
};

export default reducer;
