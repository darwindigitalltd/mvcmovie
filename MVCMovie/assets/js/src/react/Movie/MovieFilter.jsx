import React, {useState} from 'react';
import {connect} from 'react-redux';
import setGenre from './actions';


const mapStateToProps = (state) => {
  return {
    genre: state.genre
  };
};

const MovieFilter = (props) => {
  const movies = JSON.parse(props.movies);

  const genres = Array.from(new Set(movies.map((movie) => movie.genre)));

  const [selectedGenre, setSelectedGenre] = useState('');

  return (
    <div className='create-new'>
      <p>
        <a href='/Movies/Create'>Create New</a>
      </p>

      <p className='filter'>
        <select className={'filter__filter-box'} value={selectedGenre} onChange={e => {
          setSelectedGenre(e.target.value);
          props.dispatch(setGenre(e.target.value));
        }}>

          <option value={''}>All</option>
          {genres.map((genres) => {
            return <option value={genres}>{genres}</option>;
          })}

        </select>
      </p>
    </div>
  );
};

export default connect(mapStateToProps)(MovieFilter);
