import React from 'react';
import MovieFilter from './MovieFilter';
import MovieTable from './MovieTable';

const Index = (props) => {
  return (
    <React.Fragment>
      <MovieFilter {...props} />
      <MovieTable {...props} />
    </React.Fragment>
  );
};
export default Index;
