import {combineReducers} from 'redux-immutable';
import movies from '../Movie/reducer';

const rootReducer = combineReducers({
  /* import your reducers above and add them here*/
  movies
});

export default rootReducer;
